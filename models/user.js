var mongoose = require('mongoose');
var crypto = require('crypto');

var Schema = new mongoose.Schema({
    email: String,
    password: String,
    fullName: String,
    image: String,
    social: {
        googleId: String
    }
});
Schema.methods.hashPassword = function (password) {
    var hash = crypto.createHmac('sha512', 'saltVeryStrong');
    hash.update(password);
    return hash.digest('hex');
};

Schema.methods.isValidPassword = function (password) {
    return this.password === this.hashPassword(password);
};

var User = mongoose.model('User', Schema, 'users');

module.exports = User;