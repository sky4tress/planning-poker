const express = require('express');
const router = express.Router();

module.exports = function (server) {
    const io = require('socket.io')(server);
    io.on('connection', function (socket) {
        socket.broadcast.emit('notification', { message: 'A new user joined!' });
        socket.on('disconnect', function () {
            socket.broadcast.emit('notification', { message: 'Player left the channel!' });
        });
    });


};
