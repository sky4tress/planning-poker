import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './components/app.vue';
import Dashboard from './components/dashboard.vue';
import Element from 'element-ui';
import axios from 'axios';
import {loginUser} from './security';
import Cookies from 'js-cookie';
import io from 'socket.io-client';

import 'element-ui/lib/theme-default/index.css';
import "font-awesome-webpack-sass";
import 'bootstrap-grid';

Vue.use(Element);
Vue.use(VueRouter);
Vue.prototype.$http = axios;
Vue.prototype.$socket = io();

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: App,
            name: 'home',
            beforeEnter: (to, from, next) => {
                if (document.cookie.length > 0) {
                    const access_token = Cookies.get('access_token');
                    const userId = Cookies.get('user_id');
                    Cookies.remove('access_token');
                    Cookies.remove('user_id');
                    return loginUser(access_token, userId, () => {
                        next({name: "dashboard"});
                    });
                }

                const tokenData = JSON.parse(window.localStorage.getItem('authUser'));
                if (tokenData && tokenData.access_token) {
                    return next({name: "dashboard"});
                }
                next();
            }
        },
        {
            path: '/dashboard',
            component: Dashboard,
            name: 'dashboard',
            meta: {requireAuth: true},
            children: [
                {
                    path: '/logout',
                    name: 'logout',
                    beforeEnter: (to, from, next) => {
                        window.localStorage.removeItem('authUser');
                        return next({name: 'home'});
                    }
                },
                {
                    path: 'create',
                    component: require('./components/create.vue'),
                    name: 'create'
                }
            ]
        },
        {path: '*', redirect: '/'}
    ]// short for routes: routes
});

router.beforeEach((to, from, next) => {
    if (to.meta.requireAuth) {
        const tokenData = JSON.parse(window.localStorage.getItem('authUser'));
        if (!tokenData || !tokenData.access_token) {
            return next({name: "home"});
        }
    }
    next();
});

router.beforeEach((to, from, next) => {
    if (typeof to.meta.title !== 'undefined'){
        document.title = to.meta.title
    }
    next()
});


new Vue({
    el: '#app',
    router: router
});