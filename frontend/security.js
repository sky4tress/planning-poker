import axios from 'axios';
export const authUser = {};

export const getUser = function () {
    return JSON.parse(window.localStorage.getItem('authUser'));
};

export const getHeaders = function(){
    const tokenData = getUser();
    return {
        'Accept' : 'application/json',
        'Authorization': 'Bearer ' + tokenData.access_token
    };
};

export const loginUser = function (token, userId, callback) {
    authUser.access_token = token;
    authUser.id = userId;
    window.localStorage.setItem('authUser', JSON.stringify(authUser));
    axios.get('/users/' + userId, {headers: getHeaders()}).then(response => {
        authUser.email = response.data.email;
        window.localStorage.setItem('authUser', JSON.stringify(authUser));
        callback();
    });
};