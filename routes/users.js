const express = require('express');
const router = express.Router();
const User = require('../models/user');
const expressJwt = require('express-jwt');
const authenticate = expressJwt({secret : 'server secret'});




/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.get('/:id', authenticate, function (req, res, next) {
    User.findById(req.params.id, function (err, user) {
        if (err) {
            res.send(err);
        }
        if (!user) {
            res.status(404).send('Sorry cant find that!');
        }
        res.status(200).json(user);
    });
});

module.exports = router;
