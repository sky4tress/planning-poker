var express = require('express');
var router = express.Router();

var passport = require('passport');
var LocalStrategy = require('passport-local');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var User = require('../models/user');

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function (email, password, done) {
        User.findOne({email: email}, function (err, user) {
            if (user && user.isValidPassword(password)) {
                done(null, user);
            } else {
                done(null, false);
            }
        });
    }
));

passport.use(new GoogleStrategy({
        clientID: '157447477749-unro0fllqbui5gg2993lnpctono90f2f.apps.googleusercontent.com',
        clientSecret: 'otcmbBtg9ExnkCVnzwhrt5Wn',
        callbackURL: "http://vueapp.dev:3000/auth/google/callback"
    },
    function (accessToken, refreshToken, profile, done) {
        User.findOne({
            'social.googleId': profile.id
        }, function(err, user) {
            if (err) {
                return done(err);
            }
            //No user was found... so create a new user with values from Facebook (all the profile. stuff)
            if (!user) {
                user = new User({
                    fullName: profile.displayName,
                    email: profile.emails[0].value,
                    social: {googleId: profile.id}
                });
                user.save(function(err) {
                    if (err) console.log(err);
                    return done(err, user);
                });
            } else {
                //found user. Return
                return done(err, user);
            }
        });
    }
));

router.use(passport.initialize());

router.get('/auth/google',
    passport.authenticate('google', {
            session: false,
            scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email']
        }
    ));


router.get('/auth/google/callback',
    passport.authenticate('google', {failureRedirect: '/', session: false}),
    function (req, res) {
    console.log(req);
        res.cookie('access_token', generateToken(req.user.id), { maxAge: 3000 });
        res.cookie('user_id',req.user.id, { maxAge: 3000 });
        // Successful authentication, redirect home.
        res.redirect('/');
    });
router.post('/auth', passport.authenticate(
    'local', {
        session: false
    }), serialize, generateTokenMiddleware, respond);

var jwt = require('jsonwebtoken');

var db = {
    updateOrCreate: function (user, cb) {
        // db dummy, we just cb the user
        cb(null, user);
    }
};

function serialize(req, res, next) {
    db.updateOrCreate(req.user, function (err, user) {
        if (err) {
            return next(err);
        }
        // we store the updated information in req.user again
        req.user = user.id;
        next();
    });
}
function generateToken(id) {
    return jwt.sign({
        id
    }, 'server secret', {
        expiresIn: '2h'
    });
}
function generateTokenMiddleware(req, res, next) {
    req.token = generateToken(req.user.id);
    next();
}

function respond(req, res) {
    console.log('here');
    res.status(200).json({
        user: req.user,
        token: req.token
    });
}

module.exports = router;